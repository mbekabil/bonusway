const fetch = require("node-fetch");
const BASE_URL = "https://static-api.prod.bonusway.com";

const requestStoresRec = (limit, offset, collectedStores) => 
    fetch(`${BASE_URL}/api/16/campaigns_limit_${limit}_offset_${offset}_order_popularity.json`)
    .then((response) =>  response.json())
    .then( (json) => {
        const nextOffset = limit + offset;
        const totalItems = Number(json.total);
        if(nextOffset < totalItems) {  
            const filterItems = json.items
                // commissions over 2.25 %
                .filter((item, index) => item.commission.max.amount > 2.25 && (nextOffset + index) < totalItems)
                .map(store => store.title);
            
            return requestStoresRec(limit, nextOffset, [...collectedStores, ...filterItems]);
        }

        return collectedStores
    })
    .catch((err) => {
        throw err
    });


(async () => {
    try {
        const res = await requestStoresRec(10, 0, []);
        console.log(res.sort());
    } catch (err) {
        console.log(`Error: ${err}`)
    }
})();

