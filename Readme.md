
# 1. How to run the solution code

The solution is written in Javascript. The output will be logged in command line. node-fetch library is used to bring `window.fetch` to Node.js

Run the following commands to see the solution 

``` cmd
npm install
```

Then

``` cmd
npm start
```

The script will read the data from bonusway static api and returns store titles (in case sensitive alphabetical order) with with commissions over 2.25%.  

# 2. Design task to utilize bonusway API efficiently

![Architectural design diagram](./img/design-diagram.png)

As it is shown in the above diagram, different kind of data cacheing mechanisims and AWS services have used to mitigate the performance issues of the backend api. Some of the solutions are described below 

## Client side cache

The single page web app can use various client side caching mechanisms

## CloudFront + S3 bucket

Static contents of the response data (as described above in Task 1) can be stored in S3 bucket. S3 is prefered as it scales automatically with comparatively less cost.

CloudFront as a content delivery network (CDN) will help to deliver static and dynamic web contents with worldwide network of data centers (Edge Locations). It is more efficient than accessing the S3 bucket directly, as the data can be cached to the nearest location and served faster. Moreover, CloudFront with S3 will give us more option to secure our content (like geo-restrictions, signed URLs, and signed cookies, to further constrain access to the content following different criteria).  

## API Gateway

Used to trigger the corresponding Lambda to serve the request

## Lambda

Used to call bonusway API, process the data (cache it) and response the data back to the client.

## ElastiCache

Used to store data and share across different functions. Here in the above design, ElastiCache for Memcached is preferred as it is simple and cost efficient.
